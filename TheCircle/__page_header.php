<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--    <title>--><? //= isset($title) ? $title : '小新的書店' ?><!--</title>-->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/jquery-ui-git.css">
    <link rel="stylesheet" href="colorbox/example1/colorbox.css">

    <script src="https://use.fontawesome.com/59c18704f5.js"></script>

    <script src="js/jquery-3.2.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="https://code.jquery.com/ui/jquery-ui-git.js"></script>
    <script src="colorbox/jquery.colorbox.js"></script>
</head>
<style>
    * {
        box-sizing: border-box;
    }
<<<<<<< HEAD

    body > header {
        text-align: center;
    }

    body {
        font-size: 18px;
        font-family: 'Cabin', '微軟正黑體';
        line-height: 150%;
    }

    nav.guide {
        text-align: right;
        max-width: 1600px;
        margin: auto;
        margin-top: 10px;
        display: block;
    }

    nav.guide ul {
        padding: 0;
        margin: 0;
        display: inline-block;
        zoom: 1;
        margin-right: 20px;
    }

    ul > li > a {
        text-decoration: none;
=======
    body>header{
        text-align:center;
    }
    body{
        font-size:18px;
        font-family: 'Cabin','微軟正黑體';
        line-height:150%;
    }
    nav.guide{
        text-align:right;
        max-width:1600px;
        margin:auto;
        margin-top:30px;
        display:block;
    }
    nav.guide ul{
        padding:0;
        margin:0;
        display:inline-block;
        zoom:1;
        margin-right:20px;
    }
    ul>li>a{
        text-decoration:none;
>>>>>>> lynn
        border: none;
    }

    nav > ul > li {
        list-style-type: none;
        position: relative;
        /*float: left;*/
    }

    nav > ul > li > a:hover {
        opacity: 1;
        text-decoration: none;
    }

    nav.guide > ul > li:nth-child(1) > a:after, nav.guide > ul > li:nth-child(2) > a:after, nav.guide > ul > li:nth-child(3) > a:after {
        content: '／';
    }
    nav.guide>ul>li{
        float:left;
    }

    nav.stroke ul li a,
    nav.fill ul li a {
        position: relative;
    }

    nav.stroke ul li a:after, nav.fill ul li a:after {
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        width: 0%;
        content: '.';
        color: transparent;
        background: #aaa;
        height: 1px;
    }

    nav.stroke ul li a:hover:after {
        width: 100%;
    }

    nav.fill ul li a {
        transition: all 2s;
    }

    nav.fill ul li a:after {
        text-align: left;
        content: '.';
        margin: 0;
        opacity: 0;
    }

    nav.fill ul li a:hover {
        color: #fff;
        z-index: 1;
    }

    nav.fill ul li a:hover:after {
        z-index: -10;
        animation: fill 1s forwards;
        -webkit-animation: fill 1s forwards;
        -moz-animation: fill 1s forwards;
        opacity: 1;
    }

    nav.stroke {
        text-align: center;
        width: 80%;
        margin: 0 auto;
        background: #fff;
        padding: 20px 0 0 0;
        box-shadow: 0px 1px 0px #C9E2E0;
    }

    nav.stroke ul {
        display: inline-block;
        list-style: none;
        text-align: center;
        padding: 0;
    }

    nav.stroke ul li {
        display: inline-block;
    }

    nav.stroke ul li a {
        font-size: 18px;
        display: block;
        padding: 15px 20px 5px 20px;
        text-decoration: none;
        color: #000000;
        text-transform: uppercase;
        margin: 0 10px;
    }

    nav.stroke ul li a,
    nav.stroke ul li a:after,
    nav.stroke ul li a:before {
        transition: all 0.5s;
    }

    nav.stroke ul li a:hover {
        color: #555;
    }

    .fa-shopping-cart {
        font-size: 20px;
    }

    /*----------------footer-----------------*/

    .foottitle {
        font-size: 16px;
        font-weight: bold;
    }

    .row {
        width: 100%;
        min-width: 755px;
        margin: 0 auto;
        overflow: hidden;
        float:left;
    }

    .row .twocol {
        width: 13.45%
    }
    .col{
        float:left;
    }
    .c-hamburger{
        display:none;

    }
    .c-hamburger--htx {
        background-color: #ff3264;
    }

    .c-hamburger--htx span {
        transition: background 0s 0.3s;
    }

    .c-hamburger--htx span::before,
    .c-hamburger--htx span::after {
        transition-duration: 0.3s, 0.3s;
        transition-delay: 0.3s, 0s;
    }

    .c-hamburger--htx span::before {
        transition-property: top, transform;
    }

    .c-hamburger--htx span::after {
        transition-property: bottom, transform;
    }

    /* active state, i.e. menu open */
    .c-hamburger--htx.is-active {
        background-color: #cb0032;
    }

    .c-hamburger--htx.is-active span {
        background: none;
    }

    .c-hamburger--htx.is-active span::before {
        top: 0;
        transform: rotate(45deg);
    }

    .c-hamburger--htx.is-active span::after {
        bottom: 0;
        transform: rotate(-45deg);
    }

    .c-hamburger--htx.is-active span::before,
    .c-hamburger--htx.is-active span::after {
        transition-delay: 0s, 0.3s;
    }
    @media all and (max-width: 600px){
        header{
            padding:20px 0;
            height:75px;
        }
        .c-hamburger {
            display: block;
            position: relative;
            overflow: hidden;
            margin: 0 auto;
            padding: 0;
            width: 96px;
            height: 96px;
            font-size: 0;
            text-indent: -9999px;
            appearance: none;
            box-shadow: none;
            border-radius: none;
            border: none;
            cursor: pointer;
            transition: background 0.3s;
        }

        .c-hamburger:focus {
            outline: none;
        }
        .c-hamburger span {
            display: block;
            position: absolute;
            top: 44px;
            left: 18px;
            right: 18px;
            height: 8px;
            background: white;
        }

        .c-hamburger span::before,
        .c-hamburger span::after {
            position: absolute;
            display: block;
            left: 0;
            width: 100%;
            height: 8px;
            background-color: #fff;
            content: "";
        }

        .c-hamburger span::before {
            top: -20px;
        }

        .c-hamburger span::after {
            bottom: -20px;
        }
        nav.stroke{
            display:none;
        }
        nav.stroke ul li{
            background:aliceblue;
        }
        nav.guide{
            text-align:center;
        }
    }
</style>
<body>
<header>
    <nav class="guide">
        <ul class="profile">
            <li><a href="">登入</a></li>
            <li><a href="">會員中心</a></li>
            <li><a href="">收藏清單</a></li>
            <li><a href="">購物車<i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
        </ul>
    </nav>
    <h1 class="logo">
        <a href=""><img src="images/LOGO-2.png" alt="THE CIRCLE"></a>
    </h1>
    <div>
        <button class="c-hamburger c-hamburger--htx">
            <span>toggle menu</span>
        </button>
    </div>
    <script>
        $('.c-hamburger').click(function(){
            if($(this).hasClass('is-active')){
                $(this).removeClass('is-active');
                $('nav.stroke').fadeOut()
            }else{
                $(this).addClass('is-active');
                $('nav.stroke').fadeIn();
                $('nav.stroke ul li').css('display', 'block')
            }

        })
    </script>
    <nav class="stroke">
        <ul>
            <li><a href="#">最新優惠</a></li>
            <li><a href="#">全部商品</a></li>
            <li><a href="#">形象特輯</a></li>
            <li><a href="#">關於我們</a></li>
            <li><a href="#">客製服務</a></li>
        </ul>
    </nav>
</header>
</body>
</html>