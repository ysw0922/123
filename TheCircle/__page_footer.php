<!DOCTYPE html>
<html style="background-color:white">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="footer, address, phone, icons" />

	<title>Footer With Address And Phones</title>

<!--	<link rel="stylesheet" href="css/demo.css">-->
	<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">

</head>

	<body>

		<!-- The content of your page would go here. -->

		<footer class="footer-distributed">

			<div class="footer-left">

				<h3><a href=""><img src="images/LOGO-2.png"></a></h3>

				<p class="footer-links">
				<span>客服信箱 123@456.com</span>
				<br>
				<span>客服電話 02-3456-7890</span>
				<br>
				<span>服務時間 9:00AM~21:00AM</span>
				<br>
				</p>
			</div>

			<div class="footer-center">

				<div class="payment">
					<p>PAYMENT</p>
					<h4><img src="http://fs.vacanza.tw/upload/harddisc/ckf_file_1480493169_97389.jpg"></h4>
				</div>

				<div class="delivery">
					<p>DELIVERY</p>
					<h4><img src="http://fs.vacanza.tw/upload/harddisc/ckf_file_1480492972_59695.jpg"></h4>
				</div>

			</div>

			<div class="footer-right">

				<p class="footer-company-about">
					<span>FOLLOW US</span>
					
				</p>

				<div class="footer-icons">

					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-github"></i></a>

				</div>

			</div>

		</footer>
		<footer class=copyright>
			<p>COPYRIGHT &copy; 2017 THE CIRCLE ALL RIGHTS RESERVED.</p>
		</footer>

	</body>

</html>
